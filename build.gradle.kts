import org.jetbrains.compose.compose
import org.jetbrains.compose.desktop.application.dsl.TargetFormat
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile


plugins {
    kotlin("jvm") version "1.7.10"
    id("org.jetbrains.compose") version "1.4.0"
    kotlin("plugin.serialization") version "1.7.10"
    id("io.gitlab.arturbosch.detekt") version("1.20.0")
}

group = "dev.trito"
// artifact = "sasaga"
version = "0.1"

repositories {
    google()
    mavenCentral()
    maven("https://maven.pkg.jetbrains.space/public/p/compose/dev")
}

dependencies {
    implementation(compose.desktop.currentOs)
    implementation(compose.material3)
    testImplementation(kotlin("test"))

    implementation("org.seleniumhq.selenium:selenium-java:4.8.1")
    implementation("io.github.bonigarcia:webdrivermanager:5.3.2")

    implementation ("io.github.microutils:kotlin-logging-jvm:2.1.21")
    implementation("ch.qos.logback:logback-classic:1.2.10")

    val voyagerVersion = "1.0.0-rc05"
    implementation("cafe.adriel.voyager:voyager-navigator:$voyagerVersion")
}

tasks.test {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "11"
}

/*application {
    mainClass.set("dev.trito.sasaga.SaSagaAppKt")
}*/

compose.desktop {
    application {
        mainClass = "dev.trito.sasaga.gui.SasagaAppKt"
        nativeDistributions {
            targetFormats(TargetFormat.Dmg, TargetFormat.Msi, TargetFormat.Deb)
            packageName = "Sasaga"
            packageVersion = "1.0.0"
        }
    }
}

detekt {
    buildUponDefaultConfig = true // preconfigure defaults
    allRules = false // activate all available (even unstable) rules.
    //config = files("$projectDir/config/detekt.yml") // point to your custom config defining rules to run, overwriting default behavior
    //baseline = file("$projectDir/config/baseline.xml") // a way of suppressing issues before introducing detekt
}


tasks.withType<io.gitlab.arturbosch.detekt.Detekt>().configureEach {
    reports {
        html.required.set(false)
        xml.required.set(true)
        txt.required.set(false)
        sarif.required.set(false)
    }
}

//jar {
//    manifest {
//        attributes "Main-Class": "dev.trito.sasaga.SaSagaApp"
//    }
//
//    from {
//        configurations.runtimeClasspath.collect { it.isDirectory() ? it : zipTree(it) }
//    }
//}