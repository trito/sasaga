package dev.trito.sasaga.driver

import dev.trito.sasaga.data.StudentGrades
import dev.trito.sasaga.data.UpdateError
import dev.trito.sasaga.nullIfEmpty
import dev.trito.sasaga.simpleChars
import org.openqa.selenium.By
import org.openqa.selenium.WebElement

class GradeTable(val studentRows : List<StudentRow>) {
    companion object{
        fun create(tableWebElement : WebElement): GradeTable {
            val webElementMatrix = tableWebElement.findElements(By.tagName("tr")).map {
                it.findElements(By.tagName("td"))
            }.filter { it.isNotEmpty() }

            val studentRows = webElementMatrix.map {
                val studentName = it.first().text
                val selectors = it.filterIndexed { i, _ -> i%2==0 && i>1 }.dropLast(1).map { MarkSelector.create(it) }
                StudentRow(studentName, selectors)
            }
            return GradeTable(studentRows)
        }
    }


    fun apply(grades: MutableMap<String, StudentGrades>): MutableList<UpdateError> {
        val errors = mutableListOf<UpdateError>()
        for(studentRow in studentRows){
            val studentGrades = grades.remove(studentRow.name.simpleChars())
            if(studentGrades==null){
                errors+= UpdateError.StudentNotFoundInGrades(studentRow.name)
                continue
            }
            errors += studentRow.applyGrades(studentGrades)

        }
        grades.keys.forEach{ errors+= UpdateError.StudentNotFoundInSaga(it)}

        return errors
    }

    fun extractGrades() : List<StudentGrades> {
        return studentRows.map{ studentRow->
            val grades = studentRow.markSelectors.map{it.value.nullIfEmpty()}
            StudentGrades(studentRow.name, grades)
        }
    }

}