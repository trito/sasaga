package dev.trito.sasaga.driver


import dev.trito.sasaga.data.StudentGrades
import dev.trito.sasaga.data.UpdateError

/**
 * Stores name and inputs for
 */
data class StudentRow(val name:String, val markSelectors: List<MarkSelector>){
    fun isFixed(uf:Int) = markSelectors[uf] is MarkSelector.Fixed
    fun applyGrade(uf: Int, grade: Int) : Boolean{
        val selector = markSelectors[uf]
        if(selector is MarkSelector.Selector) {
            selector.selector.selectByValue(grade.toString())
            return true
        } else {
            return false
        }
    }
    fun applyGrades(studentGrades: StudentGrades): MutableList<UpdateError> {
        val errors = mutableListOf<UpdateError>()
        studentGrades.grades.forEachIndexed{ i, grade ->
            if(grade!=null) {
                val gradeSet = applyGrade(i, grade.toInt())
                if(!gradeSet)
                    errors+= UpdateError.CouldNotBeSet(name, i+1)
            } else {
                if(!isFixed(i) && i!=studentGrades.averageGradeIndex){
                    errors+= UpdateError.GradeNotFound(name, i+1)
                }
            }
        }
        return errors
    }
}