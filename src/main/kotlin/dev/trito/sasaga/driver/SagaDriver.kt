package dev.trito.sasaga


import dev.trito.sasaga.driver.*
import org.openqa.selenium.*
import org.openqa.selenium.remote.RemoteWebDriver



class SagaDriver(browser : Browser = Browser.CHROME, hidden : Boolean = false) {
    val waitSecs = 3L
    val driver : RemoteWebDriver
    var iframeSelected = false

    init {
        setupDriver(browser)
        driver =  createDriver(browser, hidden)
        //driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(waitSecs));
        driver.get("https://saga.xtec.cat/entrada");
    }

    fun close(){
        driver.close()
    }

    fun iframeSelection(){
        if(!iframeSelected) {
            val iframe = driver.findElement(By.name("contingut"))
            driver.switchTo().frame(iframe);
            iframeSelected = true
        }
    }

    fun studentRows(): GradeTable {
        iframeSelection()
        val tableWebElement = driver.findElements(By.cssSelector("table"))[3]
        return GradeTable.create(tableWebElement)
    }

//    fun login(){
//        driver.findElement(By.id("user")).sendKeys(username)
//        driver.findElement(By.id("password")).sendKeys(password)
//        Actions(driver).moveToElement(driver.findElement(By.id("password"))).perform()
//        driver.findElement(By.cssSelector("input[type=\"submit\"]")).click()
//    }
}