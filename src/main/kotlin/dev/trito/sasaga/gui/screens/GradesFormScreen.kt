package dev.trito.sasaga.gui.screens

import androidx.compose.foundation.VerticalScrollbar
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.rememberScrollbarAdapter
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import dev.trito.makineta.ui.screens.FullCenteredColumn
import java.awt.Desktop
import cafe.adriel.voyager.core.screen.Screen
import cafe.adriel.voyager.navigator.LocalNavigator
import cafe.adriel.voyager.navigator.currentOrThrow
import dev.trito.sasaga.ServiceLocator
import dev.trito.sasaga.data.SasagaError
import dev.trito.sasaga.gui.LocalLoadingState
import dev.trito.sasaga.nullIfEmpty
import dev.trito.sasaga.simpleChars
import dev.trito.sasaga.ui.asMessage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class GradesFormScreen : Screen {
    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    override fun Content() {
        val navigator = LocalNavigator.currentOrThrow
        val coroutineScope = rememberCoroutineScope()
        var studentgrades by remember { mutableStateOf("") }
        val loadingState = LocalLoadingState.currentOrThrow
        var validateAverage by remember { mutableStateOf(true) }
        var errors by remember { mutableStateOf(listOf<SasagaError>()) }
        val grades = studentgrades.lines().map { it.split("\t").map { it.nullIfEmpty() } }

        FullCenteredColumn {
            if (errors.isNotEmpty()) {
                ErrorsDisplay(errors)
            }
            Button(onClick = {
                navigator.replace(MenuScreen())
            }, modifier = Modifier.padding(5.dp)) {
                Text(text = "Seleccionar una nova operació")
            }
            Spacer(modifier = Modifier.height(10.dp))
            Row {
                Text("Validar nota mòdul (valida que l'última nota sigui una nota de mòdul correcte)")
                Checkbox(validateAverage, onCheckedChange = { validateAverage = it })
            }
            Button(onClick = {
                coroutineScope.launch(Dispatchers.IO) {
                    loadingState.loading()
                    errors = ServiceLocator.sasagaInteractors.applyGrades(grades, validateAverage)
                    loadingState.endLoading()
                }
            }, modifier = Modifier.padding(5.dp)) {
                Text(text = "Introduïr")
            }
            Row(horizontalArrangement = Arrangement.spacedBy(10.dp)) {
                OutlinedTextField(
                    value = studentgrades,
                    onValueChange = { studentgrades = it },
                    modifier = Modifier.height(500.dp),
                    label = { Text("Notes estudiants") }
                )
                val studentWithGradesResult =
                    kotlin.runCatching { ServiceLocator.sasagaInteractors.mixGradesWithStudents(grades) }
                studentWithGradesResult.onSuccess { studentWithGrades ->
                    Column {
                        ServiceLocator.sasagaInteractors.students.forEach {
                            Row {
                                Text(it, modifier = Modifier.width(250.dp))
                                studentWithGrades[it.simpleChars()]?.grades?.forEach {
                                    Text(it ?: " ", Modifier.width(30.dp))
                                }
                            }
                        }
                    }
                }

            }
        }
    }


}
