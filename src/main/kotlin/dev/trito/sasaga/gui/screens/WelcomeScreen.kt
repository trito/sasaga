package dev.trito.sasaga.gui.screens

import androidx.compose.foundation.VerticalScrollbar
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.rememberScrollbarAdapter
import androidx.compose.material3.*
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import dev.trito.makineta.ui.screens.FullCenteredColumn
import java.awt.Desktop
import cafe.adriel.voyager.core.screen.Screen
import cafe.adriel.voyager.navigator.LocalNavigator
import cafe.adriel.voyager.navigator.currentOrThrow
import dev.trito.sasaga.ServiceLocator
import dev.trito.sasaga.gui.LocalLoadingState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class WelcomeScreen : Screen {
    @Composable
    override fun Content() {
        val coroutineScope = rememberCoroutineScope()
        val navigator = LocalNavigator.currentOrThrow
        var studentNames by remember { mutableStateOf("") }
        val loadingState = LocalLoadingState.currentOrThrow
        FullCenteredColumn {
            Text("Benvingut al SaSaga",
                style = MaterialTheme.typography.titleLarge,
                textAlign = TextAlign.Center,
            )
            Spacer(modifier = Modifier.height(20.dp))
            Text("El Sistema d'Administració del Sistema d'Administració i Gestió Acadèmica (Sistem d'administració del SAGA (SASAGA))",
                style = MaterialTheme.typography.titleMedium,
                textAlign = TextAlign.Center)
            Spacer(modifier = Modifier.height(20.dp))

            Text("A l'iniciar l'aplicació s'obrirà una finestra del Chrome; no la tanquis. Un cop oberta torna al SaSaga i segueix les instruccions.")

            Spacer(modifier = Modifier.height(40.dp))

            Button(onClick = {
                coroutineScope.launch(Dispatchers.IO) {
                    loadingState.loading()
                    ServiceLocator.start()
                    navigator.replace(StudentListFormScreen())
                    loadingState.endLoading()

                }

            }){
                Text(text = "Continuar")
            }

        }
    }
}
