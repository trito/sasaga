package dev.trito.enelrecord.tomatic.ui.screens



import dev.trito.jobprogress.JobProgress
import kotlinx.coroutines.channels.ReceiveChannel
import kotlinx.coroutines.flow.receiveAsFlow

sealed class Screens<out T> {
  object MenuScreen : Screens<Nothing>()
  object StudentListFromScreen : Screens<Nothing>()
  object GradesListFromScreen : Screens<Nothing>()

  class ProgressScreen(val task: String ="", private val progressChannel: ReceiveChannel<JobProgress>) : Screens<Nothing>(){
    val progressFlow get() = progressChannel.receiveAsFlow()
  }

}
