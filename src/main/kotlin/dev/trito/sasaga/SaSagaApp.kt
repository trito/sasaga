package dev.trito.sasaga

import dev.trito.sasaga.ui.SaSagaCli

fun main() {
    SaSagaCli().start()
}