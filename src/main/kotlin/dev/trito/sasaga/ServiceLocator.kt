package dev.trito.sasaga

object ServiceLocator {
    lateinit var sasagaInteractors: SasagaInteractors

    fun start(){
        sasagaInteractors = SasagaInteractors()
    }
}