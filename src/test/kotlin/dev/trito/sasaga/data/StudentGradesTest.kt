package dev.trito.sasaga.data

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

internal class StudentGradesTest {

    fun validGrades(grades: List<Int?>):Boolean{
        val stringGrades = grades.map { it?.toString() ?: "" }
        val studentGrades = StudentGrades("name", stringGrades)
        return studentGrades.hasValidAverageGrade()
    }

    @Test
    fun oneFailedPartialAndNoAverageIsValid() {
        val hasValidAverageGrade = validGrades(listOf(6,1,6,null))
        assertEquals(true, hasValidAverageGrade)
    }

    @Test
    fun oneFailedPartialAndAverageIsInValid() {
        val hasValidAverageGrade = validGrades(listOf(6,1,6,2))
        assertEquals(false, hasValidAverageGrade)
    }

    @Test
    fun allPassedPartialsAndAverageIsValid() {
        val hasValidAverageGrade = validGrades(listOf(6,9,6,5,5))
        assertEquals(true, hasValidAverageGrade)
    }

    @Test
    fun allPassedPartialsAndNoAverageIsInValid() {
        val hasValidAverageGrade = validGrades(listOf(6,9,6,5,null))
        assertEquals(false, hasValidAverageGrade)
    }

    @Test
    fun failedAverageIsInValid() {
        val hasValidAverageGrade = validGrades(listOf(6,9,6,5,4))
        assertEquals(false, hasValidAverageGrade)
    }
}