# SASAGA
Sistema d'Administració del Sistema d'Administració i Gestió Acadèmica (Sistem d'administració del SAGA (SASAGA))

## About
Simple terminal app to automatize grades introduction and verification in SAGA.

## Download
You can download the latest compiled version here:

https://gitlab.com/trito/sasaga/-/jobs/artifacts/master/browse?job=package